#! /bin/sh

ROOT=$(git rev-parse --show-toplevel)

cd $ROOT
git ls-files --exclude-standard --ignored --others .
